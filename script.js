let canvas;
let context;
let isDrawing;
let inputX = document.getElementById("x");
let inputY = document.getElementById("y");
let div = document.createElement("div");
let previousColorElement;
let previousThicknessElement;

window.onload = function () {

    canvas = document.getElementById("drawingCanvas");
    context = canvas.getContext("2d");
    changeColor('red', div);
    canvas.onmousedown = startDrawing;
    canvas.onmouseup = stopDrawing;
    canvas.onmouseout = stopDrawing;
    canvas.onmousemove = draw;
};

function changeColor(color, div) {
    console.log(context);
    context.strokeStyle = color;
    div.className = "Selected";
    if (previousColorElement != null)
        previousColorElement.className = "";
    previousColorElement = div;
}

function changeThickness(thickness, imgElement) {
    context.lineWidth = thickness;
    imgElement.className = "Selected";
    if (previousThicknessElement != null)
        previousThicknessElement.className = "";
    previousThicknessElement = imgElement;
}

function startDrawing(e) {
    isDrawing = true;
    context.beginPath();
    context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
}

function draw(e) {
    if (isDrawing === true) {
        let x = e.pageX - canvas.offsetLeft;
        let y = e.pageY - canvas.offsetTop;
        context.lineTo(x, y);
        context.stroke();
        inputX.innerText = x;
        inputY.innerText = y;
        document.getElementById("y").value = y;
        document.getElementById("x").value = x;
    }
}

function stopDrawing() {
    isDrawing = false;
}

function clearCanvas() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}